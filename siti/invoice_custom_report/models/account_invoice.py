# coding: utf-8
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

from odoo import _, api, models
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def get_origin(self, description):
        for rec in self:
            origin = ''
            for lines in rec.invoice_line_ids:
                if description == self.get_string_cfdi(lines.name):
                    origin = lines.origin
        return origin

    @api.multi
    def invoice_print(self):
        if not self.xml_signed:
            raise ValidationError(
                _('Can not print without properly stamped XML.'))
        return super(AccountInvoice, self).invoice_print()
