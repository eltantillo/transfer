# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.v.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Stock Kiosk Mode for Entrance Validation',
    'summary': 'Validate entrance of products in kiosk mode.',
    'version': '10.0.1.0.0',
    'category': 'Stock',
    'website': 'https://www.jarsa.com.mx/',
    'author': 'Jarsa Sistemas, S.A. de C.v.',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'stock_account',
        'stock_landed_costs',
    ],
    'data': [
        'data/ir_sequence_data.xml',
        'security/ir.model.access.csv',
        'views/stock_picking_interface_view.xml',
        'views/stock_picking_interface_kiosk_view.xml',
        'views/web_asset_backend_template.xml',
        'views/res_config_settings_view.xml',
    ],
    'qweb': [
        "static/src/xml/kiosk_mode.xml",
    ],
}
