.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

======================================
Stock Inteface Kiosk Mode
======================================

This application allows you to search an invoice and validate the product entrance

Configuration
=============

To configure this module, you need to:


Usage
=====

To use this module, you need to:

#. Open Kiosk mode.
#. Search the supplier invoice.
#. Validate the product quantities.
#. Process the validations.


Known issues / Roadmap
======================

* Work in progress

Bug Tracker
===========

Bugs are tracked on `GitHub Issues
<https://github.com/Jarsa/transport-management-system/issues>`_. In case of trouble, please
check there if your issue has already been reported. If you spotted it first,
help us smashing it by providing a detailed and welcomed feedback.

Credits
=======

Contributors
------------

* Oscar Garza <oscar.garza@jarsa.com.mx>


Maintainer
----------

.. image:: http://www.jarsa.com.mx/logo.png
   :alt: Jarsa Sistemas, S.A. de C.V.
   :target: http://www.jarsa.com.mx

This module is maintained by Jarsa Sistemas, S.A. de C.V.
