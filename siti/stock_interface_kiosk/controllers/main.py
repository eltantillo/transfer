# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import werkzeug.utils
from odoo import _, http
from odoo.exceptions import ValidationError
from odoo.http import request


class StockInterfaceKiosController(http.Controller):

    @http.route(['/web/stock_interface'], type='http', auth="user")
    def stock_interface(self):
        return werkzeug.utils.redirect(
            '/web#action=stock_interface_kiosk.stock_interface_kiosk_action')

    def get_backorder(self, picking, product_id, spi):
        """ Recursive method to find quantities pending
            processing in backorders

            :param picking: Backorder
            :param product_id: Stock Move product
            :return: self-method with the current backorder or the pending qty
            :rtype: self-method or float """
        if picking.state in ['done', 'cancel'] and picking.backorder_id:
            return self.get_backorder(picking.backorder_id, product_id, spi)
        moves = picking.move_lines.filtered(
            lambda m: m.product_id.id == product_id and
            m.state == 'assigned' and
            m.stock_picking_interface_line_id.stock_picking_interface_id !=
            spi)
        if not moves:
            return {
                'move_ids': request.env['stock.move'],
                'ordered_qty': 0.0,
            }
        return {
            'move_ids': moves,
            'ordered_qty': sum(moves.mapped('product_uom_qty')) - sum(
                moves.mapped('quantity_done')),
        }

    def get_stock_moves(self, spi):
        """ Method to prepare the data to create the stock picking interface line

            :param spi: Stock Picking Interface
            :return: a dictionary ready to record creation
            :rtype: dict """
        picking_obj = request.env['stock.picking']
        for line in spi.invoice_id.invoice_line_ids:
            if not line.product_id:
                continue
            if line.product_id not in spi.line_ids.mapped('product_id'):
                spi_line = spi.line_ids.create({
                    'product_id': line.product_id.id,
                    'ordered_qty': 0.0,
                    'uom_id': line.product_id.uom_id.id,
                    'stock_picking_interface_id': spi.id,
                })
                ordered_qty = 0.0
            else:
                spi_line = spi.line_ids.filtered(
                    lambda l: l.product_id == line.product_id)
                ordered_qty = spi.line_ids.filtered(
                    lambda l: l.product_id == line.product_id).ordered_qty
            # Only put the max quantity available depending of the
            # invoice line quantity
            inv_product_lines = line.invoice_id.invoice_line_ids.filtered(
                lambda l: l.product_id == line.product_id)
            invoice_total_qty = 0.0
            for inv_line in inv_product_lines:
                inv_qty = inv_line.quantity
                if inv_line.uom_id != inv_line.product_id.uom_po_id:
                    inv_qty = inv_line.uom_id._compute_quantity(
                        inv_qty, inv_line.product_id.uom_po_id)
                invoice_total_qty += inv_qty
            if ordered_qty > invoice_total_qty:
                ordered_qty = invoice_total_qty
                continue
            moves = line.purchase_line_id.move_ids.mapped(
                'move_dest_id').filtered(
                lambda m: m.picking_type_id == request.env.ref(
                    'stock.picking_type_internal'))
            for move in moves:
                if ((move.product_qty - move.quantity_done) >
                        invoice_total_qty and move.state == 'assigned'):
                    ordered_qty = invoice_total_qty
                    move.stock_picking_interface_line_id = spi_line.id
                    continue
                move_spi = (
                    move.stock_picking_interface_line_id.
                    stock_picking_interface_id)
                backorder = picking_obj.search(
                    [('backorder_id', '=', move.picking_id.id)])
                if (move.state == 'done' and backorder):
                    backorders = self.get_backorder(
                        backorder, move.product_id.id, spi)
                    if not backorders or not backorders['move_ids']:
                        continue
                    ordered_qty += backorders['ordered_qty']
                    backorders['move_ids'].write({
                        'stock_picking_interface_line_id': spi_line.id,
                    })
                elif (move.state == 'assigned' and move_spi != spi):
                    ordered_qty += (move.product_uom_qty - move.quantity_done)
                    move.stock_picking_interface_line_id = spi_line.id
            if ordered_qty == 0.0:
                spi_line.unlink()
                continue
            spi_line.ordered_qty = ordered_qty

    def prepare_spi(self, invoice):
        """ Method to prepare the data to create the stock picking interface

            :param invoice: Supplier Invoice
            :return: a dictionary ready to record creation
            :rtype: dict """
        return {
            'name': request.env.ref(
                'stock_interface_kiosk.stock_picking_interface_sequence'
            ).next_by_id(),
            'invoice_id': invoice.id,
        }

    @http.route(['/stock_interface/search_invoice'], type='json', auth="user")
    def search_invoice(self, number):
        if not number:
            raise ValidationError(
                _('Please enter a invoice number or invoice reference'))
        res = {}
        invoice_obj = request.env['account.invoice']
        spi_obj = request.env['stock.picking.interface']
        invoice = invoice_obj.search(
            [('type', '=', 'in_invoice'),
             ('state', 'in', ['open', 'paid']),
             '|', ('number', 'ilike', number), ('reference', 'ilike', number)],
            limit=1)

        if not invoice:
            raise ValidationError(_('There are not an invoice, try again.'))
        existing_spi = True
        if not invoice.stock_picking_interface_id:
            spi = spi_obj.create(self.prepare_spi(invoice))
            self.get_stock_moves(spi)
            if not spi.line_ids:
                return _('There not any moves to be processed.')
            invoice.stock_picking_interface_id = spi.id
            existing_spi = False
        if invoice.stock_picking_interface_id.state == 'done':
            return (
                _('The invoice %s is already processed in'
                    ' the picking interface %s.<br/><br/>'
                    'The pending pickings are listed in the invoice chatter.'
                    'This pickings must be validated manually.') % (
                    invoice.number,
                    invoice.stock_picking_interface_id.name))

        spi = invoice.stock_picking_interface_id
        res.update(spi.read_spi())
        res['existing_spi'] = existing_spi
        return res
