odoo.define('stock_interface_kiosk.kiosk_mode', function (require) {
    "use strict";

    var core = require('web.core');
    var Model = require('web.Model');
    var Widget = require('web.Widget');
    var Session = require('web.session');
    var mobile = require('web_mobile.rpc');
    var BarcodeHandlerMixin = require('barcodes.BarcodeHandlerMixin');

    var QWeb = core.qweb;
    var _t = core._t;

    var StockInterfaceKiosk = Widget.extend(BarcodeHandlerMixin, {
        events: {
            "click .o_stock_kiosk_mobile_barcode": 'open_mobile_scanner',
            'click #search_btn': 'search_invoice',
            'keyup #search_product': 'on_filter_keyup',
            'click #product_table table tbody tr': 'on_tr_click',
            'shown.bs.modal #serial_number_modal': function (e) { $(e.currentTarget).find('[autofocus]').focus(); },
            'keyup #serial_number_modal': function (e) {if(e.which == 13) { this.action_set_serial_number(); }},
            'hidden.bs.modal #product_detail_modal': function() { $('#product_detail_modal').attr('barcode_active', ''); },
            'click #validate_btn': 'action_validate',
            'click #cancel_btn': 'action_cancel',
            'click .action_buttons': 'action_resume_interface',
            'click #btn_sn_confirm': 'action_set_serial_number',
            'click #btn_validate_spi': 'action_validate_spi',
            'click #btn_cancel_spi': 'action_cancel_spi',
            'click #btn_restart': 'action_restart',
            'click #btn_clear_search_input': function () {this.$el.find('#search_product').val(""); this.on_filter_keyup();},
            'click #button_add_qty': 'button_add_qty',
            'click #button_remove_qty': 'button_remove_qty',
        },

        init: function (parent, action) {
            this._super;
            BarcodeHandlerMixin.init.apply(this, arguments);
            $("body").attr('id', 'stock_picking_interface_body');
        },

        start: function () {
            var self = this;
            self.session = Session;
            var res_company = new Model('res.company');
            res_company.query(['name'])
               .filter([['id', '=', self.session.company_id]])
               .all()
               .then(function (companies){
                    self.company_name = companies[0].name;
                    self.company_image_url = self.session.url('/web/image', {model: 'res.company', id: self.session.company_id, field: 'logo',});
                    self.$el.html(QWeb.render("StockInterfaceKioskMode", {widget: self}));
                });
               self.start_clock();
               self.animated = false;
            return self._super.apply(this, arguments);
        },

        search_invoice: function() {
            var self = this;
            self.rpc("/stock_interface/search_invoice", {number: $('#invoice_number').val()}).then(function(result) {
                 if (typeof result == "string") {
                    $('#div_modal_invoice_done').html(QWeb.render('InvoiceDoneModal', {msg: result}));
                    return $("#invoice_done_modal").modal('show');
                 }
                 if (result.existing_spi) {
                    $('#div_modal_existing_invoice').html(QWeb.render('ExistingInterfaceModal', {}));
                    self.spi_id = result.id;
                    return $("#existing_interface_modal").modal('show');
                 }
                 self.$el.html(QWeb.render("StockInterfaceKioskModeProductView", {spi: result, company_name: self.company_name, company_image_url: self.company_image_url}));
                 if(!mobile.methods.scanBarcode){
                    self.$el.find(".o_stock_kiosk_mobile_barcode").remove();
                 }
            })
        },

        action_resume_interface: function(e) {
            var self = this
            var action = $(e.currentTarget).attr('action');
            new Model('stock.picking.interface').call('read_spi', [self.spi_id, action])
                .then(function (result) {
                    $("#existing_interface_modal").modal('hide');
                    $(".modal-backdrop").css('display', 'none');
                    self.$el.html(QWeb.render("StockInterfaceKioskModeProductView", {spi: result, company_name: self.company_name, company_image_url: self.company_image_url}));
                    if(!mobile.methods.scanBarcode){
                        self.$el.find(".o_stock_kiosk_mobile_barcode").remove();
                    }
            });
            
        },
       
        on_filter_keyup: function () {
            var rex = new RegExp($('#search_product').val(), 'i');
            $('#table-content tr').hide();
            $('#table-content tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        },

        open_mobile_scanner: function() {
            var self = this;
            mobile.methods.scanBarcode().then(function(response){
                var barcode = response.data;
                if(barcode){
                    self.on_barcode_scanned(barcode);
                    mobile.methods.vibrate({'duration': 100});
                }else{
                    mobile.methods.showToast({'message':'Please, Scan again !!'});
                }
            });
        },

        action_set_serial_number: function() {
            var self = this;
            var active_tr = $('table #barcode').filter(function() {
                return $(this).text() == $("#product_barcode").val();
            }).parent('tr');
            var serial_numbers = active_tr.find('#serial_number').text().split(',');
            var value = $('#serial_number_input').val();

            if (!value) {
                $('#alerts_container').html(QWeb.render('AlertsTemplate', {
                    title:_t('Error!'), msg: _t('You must enter a serial number.'),
                }));
                self.animate_warning();
                $('#serial_number_input').focus();
                return false
            }
            if (serial_numbers.indexOf(value) != -1) {
                $('#alerts_container').html(QWeb.render('AlertsTemplate', {
                    title:_t('Error!'),
                    msg: _t('You have already entered this serial number before.'),
                }));
                self.animate_warning();
                $('#serial_number_input').val("");
                $('#serial_number_input').focus();
                return false

            }

            var line_id = active_tr.find('#line_id').text();
            var new_value = active_tr.find('#serial_number').text() + String(value) + ',';
            active_tr.find('#serial_number').text(new_value);
            $("#serial_number_modal").modal('hide');
            $('#serial_number_input').val("");
            self.write_model_values('stock.picking.interface.line', parseInt(line_id), {
                'serial_number': active_tr.find('#serial_number').text(),
            });
            self.render_product_modal(active_tr);
            self.add_product_qty(active_tr);
            self.render_product_modal_footer(active_tr);
        },

        cmd_read_num: function(barcode) {
            var self = this;
            switch (barcode){
                case 'CMD-READ-NUM':
                    $('.alert-danger').removeClass('hidden');
                    $('#read-number').prop("checked", true);
                    self.cmd_number = "";
                    break;
                case 'CMD-DONE':
                    if (!self.cmd_number) {
                        self.cmd_number = "0";
                    }
                    self.cmd_number = parseFloat(self.cmd_number);
                    $('#read-number').prop("checked", false);
                    self.on_barcode_scanned($('#product_detail_modal').attr('barcode_active'));

                    // Re-init Values
                    self.cmd_number = 1;
                    $('.alert-danger').addClass('hidden');
                    $("#active_cmd_number").text("");
                    break;
                case 'CMD-BACKSPACE':
                    if (self.cmd_number.length === 1) {
                        self.cmd_number = "";
                    }
                    else if (self.cmd_number) {
                        // Erase the last digit of the string
                        self.cmd_number = self.cmd_number.slice(0, -1);
                    }
                    $("#active_cmd_number").text(self.cmd_number);
                    break;
                case "CMD-1":
                case "CMD-2":
                case "CMD-3":
                case "CMD-4":
                case "CMD-5":
                case "CMD-6":
                case "CMD-7":
                case "CMD-8":
                case "CMD-9":
                case "CMD-0":
                    if(typeof self.cmd_number == 'number'){
                        self.cmd_number = ""
                    }
                    self.cmd_number = self.cmd_number + String(parseInt(barcode.split("CMD-")[1]));
                    $("#active_cmd_number").text(self.cmd_number);
                    break;
                default:
                    if(isNaN(parseFloat(barcode))){
                        $('#alerts_container').html(QWeb.render('AlertsTemplate', {
                            title:_t('Error!'),
                            msg: _t('The scanned code is not a number, if you want to process another product, first end the Read Number Mode.'),
                        }));
                        self.animate_warning();
                    }
                    break;
            }
        },

        animate_warning: function() {
            var self = this;
            $("#alerts_container").animate({opacity: 1.0}, 400, "swing");
            $("#alerts_container").css('z-index', '10000');
            if(!self.animated) {
                self.hide_warning = clearTimeout(self.hide_warning);
                self.hide_warning = setTimeout(function() {
                    $('#alerts_container').animate({opacity: 0.0}, 400, "swing");
                    $("#alerts_container").css('z-index', '100');
                    self.animated = false;
                }, 5000);
            }
            self.animated = !self.animated;
        },

        on_barcode_scanned: function(barcode) {
            var self = this;
            barcode = barcode.trim();
            if ((['CMD-READ-NUM', 'CMD-BACKSPACE', 'CMD-DONE'].indexOf(barcode) != -1 || $('#read-number').prop("checked")) && $('#product_detail_modal').attr('barcode_active')) {
                return self.cmd_read_num(barcode);
            }

            var active_tr = $('table #barcode').filter(function() {
                return $(this).text() == barcode;
            }).parent('tr');
            if (!active_tr.length){
                $('#alerts_container').html(QWeb.render('AlertsTemplate', {title:_t('Error!'), msg: _t('Barcode ') + barcode + _t(' not found.')}));
                return self.animate_warning();
            }

            if ($('#product_detail_modal').attr('barcode_active') == barcode) {
                // Serial number handler
                if (active_tr.find('#need_serial_number').text() == 'true') {
                    $('#product_barcode').val(barcode);
                    return $("#serial_number_modal").modal('show');
                }
                var quantities = self.add_product_qty(active_tr, self.cmd_number);
                if (!quantities) {
                    return true
                }

                active_tr.find('#qty_done').text(quantities['new_qty_done']);
                active_tr.find('#qty_todo').text(quantities['new_qty_todo']);
                $('#qty_done_modal').text(quantities['new_qty_done']);
                $('#qty_todo_modal').text(quantities['new_qty_todo']);
                return true;
            }
            $('#product_detail_modal').attr('barcode_active', barcode);

            // Reset the TODO quantities
            $('table #qty_todo').text('0');
            self.render_product_modal(active_tr);
            self.render_product_modal_footer(active_tr);
            // Show Modal
            if (!$('#product_detail_modal').hasClass('in')) {
                $('#product_detail_modal').modal('show');
            }
            else {
                $('#product_detail_modal .modal-body').toggle("explode");
                $('#product_detail_modal .modal-body').toggle("explode");
            } 
            // Serial number handler
            if (active_tr.find('#need_serial_number').text() == 'true') {
                $('#product_barcode').val(barcode);
                return $("#serial_number_modal").modal('show');
            }
            self.add_product_qty(active_tr, self.cmd_number);
            self.render_product_modal(active_tr);
            self.render_product_modal_footer(active_tr);

        },

        on_tr_click: function(e) {
            var self = this;
            var active_tr = $(e.currentTarget);
            $('#product_detail_modal').modal('show');
            self.render_product_modal(active_tr);
            self.render_product_modal_footer(active_tr);
        },

        add_product_qty: function(active_tr, qty_to_sum=1) {
            var self = this;
            var ordered_qty = active_tr.find('#ordered_qty').text();
            var qty_done = active_tr.find('#qty_done').text();
            var qty_todo = active_tr.find('#qty_todo').text();
            var new_qty_todo = parseFloat(qty_todo) + qty_to_sum;
            if (new_qty_todo < 0) {
                new_qty_todo = 0;
            }
            var new_qty_done = parseFloat(qty_done) + qty_to_sum;
            if (new_qty_done < 0) {
                new_qty_done = 0;
            }
            var line_id = active_tr.find('#line_id').text();
            if (new_qty_done <= parseFloat(ordered_qty)) {
                active_tr.find('#qty_todo').text(new_qty_todo);
                active_tr.find('#qty_done').text(new_qty_done);
                // Write values on model
                self.write_model_values('stock.picking.interface.line', parseInt(line_id), {
                    'qty_done': new_qty_done,
                });
            }
            else if (new_qty_done > parseFloat(ordered_qty)) {
                new_qty_done = ordered_qty;
                new_qty_todo = qty_todo;
                 $('#alerts_container').html(QWeb.render('AlertsTemplate', {
                        title:_t('Error!'),
                        msg: _t('The quantity done must be lower or equal than the quantity ordered.'),
                }));
                self.animate_warning();
                return false;
            }
            return {
                'new_qty_done': new_qty_done,
                'new_qty_todo': new_qty_todo,
            };
        },

        render_product_modal: function(active_tr) {
            var self = this;
            $('#product_detail_modal').attr('barcode_active', active_tr.find('#barcode').text());
            var product_image = active_tr.find('#product_image').text();
            var default_code = active_tr.find('#default_code').text();
            var name = active_tr.find('#name').text();
            var uom_id = active_tr.find('#uom_id').text();
            var description = active_tr.find('#description').text();

            $("#product_detail_modal .modal-body").html(QWeb.render("StockInterfaceProductDetailBody", {
                default_code: default_code,
                name: name,
                uom_id: uom_id,
                description: description,
            }));
            if(product_image != "false") {
                $("#product_img").attr("src", 'data:image/jpeg;base64,' + product_image);
            }
            else {
                $("#product_img").attr("src", '/stock_interface_kiosk/static/src/img/no_image.png');
            }
            return true;
        },

        render_product_modal_footer: function(active_tr) {
            var ordered_qty = active_tr.find('#ordered_qty').text();
            var new_qty_todo = active_tr.find('#qty_todo').text();
            var new_qty_done = active_tr.find('#qty_done').text();
            $("#product_detail_modal .modal-footer").html(QWeb.render("StockInterfaceProductDetailFooter", {
                ordered_qty: ordered_qty,
                qty_done: new_qty_done,
                qty_todo: new_qty_todo,
            }));
            self.cmd_number = 1;
        },

        write_model_values: function(model, recs, values) {
            // General Method to write any value into any model
            new Model(model)
            .call("write", [recs, values])
            .then(function (result) {
                return result
            });
        },

        button_add_qty: function(e) {
            var active_tr = $('table #barcode').filter(function() {
                return $(this).text() == $('#product_detail_modal').attr('barcode_active');
            }).parent('tr');
            // Serial number handler
            if (active_tr.find('#need_serial_number').text() == 'true') {
                $('#product_barcode').val(active_tr.find('#barcode').text());
                return $("#serial_number_modal").modal('show');
            };
            var quantities = this.add_product_qty(active_tr);
            active_tr.find('#qty_done').text(quantities['new_qty_done']);
            active_tr.find('#qty_todo').text(quantities['new_qty_todo']);
            $('#qty_done_modal').text(quantities['new_qty_done']);
            $('#qty_todo_modal').text(quantities['new_qty_todo']);
            e.currentTarget.blur();
            e.currentTarget.style.outline = 'none';
        },

        button_remove_qty: function(e){
            var active_tr = $('table #barcode').filter(function() {
                return $(this).text() == $('#product_detail_modal').attr('barcode_active');
            }).parent('tr');
            // Serial number handler
            if (active_tr.find('#need_serial_number').text() == 'true') {
                 $('#alerts_container').html(QWeb.render('AlertsTemplate', {
                    title:_t('Error!'),
                    msg: _t('You do not decrement the quantity done of this product because already has serial number.'),
                }));
                this.animate_warning();
                return false
            };
            var quantities = this.add_product_qty(active_tr, -1);
            active_tr.find('#qty_done').text(quantities['new_qty_done']);
            active_tr.find('#qty_todo').text(quantities['new_qty_todo']);
            $('#qty_done_modal').text(quantities['new_qty_done']);
            $('#qty_todo_modal').text(quantities['new_qty_todo']);
            e.currentTarget.blur();
            e.currentTarget.style.outline = 'none';
        },

        action_validate: function() {
            var missing_lines = new Array();
            var missing_tr = $('#table-content tr').filter(function() {
                return $(this).find('#ordered_qty').text() != $(this).find('#qty_done').text()
            });
            var message = _t('Are you sure to validate this entrance?');
            if (missing_tr.length){
                var message = _t(
                    'Are you sure to validate this entrance?. <br/> The following products are not complete yet.');
                $.each(missing_tr, function(key, value){
                    missing_lines.push({
                        'product_id': '[' + $(this).find('#default_code').text() + '] ' + $(this).find('#name').text(),
                        'missing_qty': parseFloat($(this).find('#ordered_qty').text()) - parseFloat($(this).find('#qty_done').text()),
                    });
                });
            }
            this.render_actions_modal(message, 'validate', missing_lines);
        },

        action_cancel: function() {
            this.render_actions_modal(_t('Are you sure to cancel this entrance?'), 'cancel');
        },

        render_actions_modal: function(message, action, missing_lines=new Array()) {
            $('#actions_modal_container').html(QWeb.render('ActionsModal', {message: message, missing_lines: missing_lines}));
            if(action === 'validate') {
                if (missing_lines.length) {
                    $('#missing_products_table').removeClass('hidden');
                }
                $('#button_action').prop('id', 'btn_validate_spi');
            }
            else {
                $('#button_action').prop('id', 'btn_cancel_spi');
            }
            $('#actions_modal').modal('show');
        },

        action_validate_spi: function() {
            var self = this;
            var spi = $('#spi_id').text();
            new Model('stock.picking.interface')
            .call("action_validate", [parseInt(spi)])
            .then(function (result) {
                $('#actions_modal').modal('hide');
                $('#success_modal_container').html(QWeb.render('SuccessModal', {spi: $('#spi_name').text()}));
                $('#success_modal').modal('show');
            });
        },
        
        action_cancel_spi: function() {
            var self = this;
            var spi = $('#spi_id').text();
            new Model('stock.picking.interface')
            .call("action_cancel", [parseInt(spi)])
            .then(function (result) {
                return location.reload();
            });
        },

        action_restart: function() {
            return location.reload();
        },

        start_clock: function() {
            this.clock_start = setInterval(function() {
                this.$(".o_stock_interface_kiosk_clock").text(new Date().toLocaleTimeString(navigator.language, {
                    day: '2-digit',
                    month: '2-digit',
                    year:'numeric',
                    hour: '2-digit',
                    minute:'2-digit',
                    second:'2-digit',
                }));
            }, 500);
            // First clock refresh before interval to avoid delay
            this.$(".o_stock_interface_kiosk_clock").text(new Date().toLocaleTimeString(navigator.language, {
                day: '2-digit',
                month: '2-digit',
                year:'numeric',
                hour: '2-digit',
                minute:'2-digit',
                second:'2-digit',
            }));
            this.clock_start = setInterval(function() {
                this.$(".o_stock_interface_kiosk_clock_product").text(new Date().toLocaleTimeString(navigator.language, {
                    day: '2-digit',
                    month: '2-digit',
                    year:'numeric',
                    hour: '2-digit',
                    minute:'2-digit',
                    second:'2-digit',
                }));
            }, 500);
            // First clock refresh before interval to avoid delay
            this.$(".o_stock_interface_kiosk_clock_product").text(new Date().toLocaleTimeString(navigator.language, {
                day: '2-digit',
                month: '2-digit',
                year:'numeric',
                hour: '2-digit',
                minute:'2-digit',
                second:'2-digit',
            }));
        },

        destroy: function () {
            clearInterval(this.clock_start);
            $("body").attr('id', '');
            this._super.apply(this, arguments);
        },
    });

    core.action_registry.add('stock_interface_kiosk', StockInterfaceKiosk);

    return StockInterfaceKiosk;

});
