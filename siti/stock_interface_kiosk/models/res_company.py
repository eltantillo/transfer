# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

from odoo import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    landed_cost_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string="Landed Cost Journal",
        help="Journal used to create stock landed cost "
        "in Stock Picking Interface",)
