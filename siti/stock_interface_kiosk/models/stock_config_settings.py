# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

from odoo import api, fields, models


class StockSettings(models.TransientModel):
    _inherit = 'stock.config.settings'

    landed_cost_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string="Landed Cost Journal",
        compute="_compute_landed_cost_journal_id",
        inverse="_inverse_landed_cost_journal_id",
        domain="[('type', '=', 'general')]",
        help="Journal used to create stock landed cost "
        "in Stock Picking Interface",)

    @api.depends('company_id')
    def _compute_landed_cost_journal_id(self):
        for rec in self:
            rec.landed_cost_journal_id = (
                rec.company_id.landed_cost_journal_id.id)

    @api.multi
    def _inverse_landed_cost_journal_id(self):
        for rec in self:
            if (rec.landed_cost_journal_id !=
                    rec.company_id.landed_cost_journal_id):
                rec.company_id.landed_cost_journal_id = (
                    rec.landed_cost_journal_id.id)
