# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class StockPickingInterface(models.Model):
    _name = 'stock.picking.interface'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char(readonly=True, required=True,)
    picking_ids = fields.Many2many(
        'stock.picking',
        string='Pickings',
        required=True,
        help='Technical field to link the pickings to the stock interface',
        compute='_compute_picking_ids',)
    picking_count = fields.Integer(
        compute='_compute_picking_count',
        string='Picking(s)',)
    invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Invoice',
        readonly=True,
        help='''Techincal field to link the stock interface
        with the supplier invoice.''',)
    date = fields.Date(
        default=fields.Date.today, readonly=True, required=True,)
    state = fields.Selection(
        [('draft', 'Draft'),
         ('done', 'Done'),
         ('cancel', 'Cancel')],
        default='draft',
        readonly=True,
        required=True,
        track_visibility='always',)
    line_ids = fields.One2many(
        comodel_name='stock.picking.interface.line',
        inverse_name='stock_picking_interface_id',
        string='Lines',
        readonly=True,)
    missing_line_count = fields.Integer(
        readonly=True,
        default=0.0,
        help='Field used to express the quantity of lines to be processed.',
        compute='_compute_missing_line_count',)
    stock_landed_cost_id = fields.Many2one(
        comodel_name='stock.landed.cost',
        string='Landed Cost',)

    @api.multi
    @api.depends('picking_ids')
    def _compute_picking_count(self):
        for rec in self:
            rec.picking_count = len(rec.picking_ids)

    @api.multi
    @api.depends('line_ids')
    def _compute_picking_ids(self):
        for rec in self:
            if rec.state == 'done':
                rec.picking_ids = rec.line_ids.mapped(
                    'move_ids.picking_id').filtered(
                    lambda p: p.state == 'done')
            else:
                rec.picking_ids = rec.line_ids.mapped('move_ids.picking_id')

    @api.multi
    def _compute_missing_line_count(self):
        for rec in self:
            rec.missing_line_count = 0.0

    @api.multi
    def read_spi_lines(self):
        self.ensure_one()
        res = []
        for line in self.line_ids:
            res.append({
                'line_id': line.id,
                'product_image': line.product_id.image,
                'description': (
                    line.product_id.description_picking or
                    line.product_id.name),
                'default_code': line.product_id.default_code or '',
                'name': line.product_id.name,
                'uom_id': line.uom_id.name,
                'ordered_qty': line.ordered_qty,
                'barcode': line.product_id.barcode,
                'qty_done': line.qty_done,
                'need_serial_number': line.need_serial_number,
                'serial_number': line.serial_number or '',
            })
        return res

    @api.multi
    def read_spi(self, action='resume'):
        self.ensure_one()
        if action != 'resume':
            self.line_ids.write({
                'qty_done': 0.0,
                'serial_number': False,
            })
        return {
            'id': self.id,
            'name': self.name,
            'date': self.date,
            'line_ids': self.read_spi_lines(),
        }

    @api.multi
    def action_cancel(self):
        self.ensure_one()
        self.invoice_id.stock_picking_interface_id = False
        self.state = 'cancel'

    @api.model
    def get_move_ordered(self, line):
        so_moves = sorted(line.move_ids.filtered(
            lambda m: m.group_id.name.startswith('SO')), key=lambda p: p.date)
        po_moves = sorted(line.move_ids.filtered(
            lambda m: m.group_id.name.startswith('PO')), key=lambda p: p.date)
        op_moves = sorted(line.move_ids.filtered(
            lambda m: m.group_id.name.startswith('OP') or
            m.group_id.name.startswith('PG')), key=lambda p: p.date)
        return so_moves + op_moves + po_moves

    @api.model
    def process_move_spo(self, line, move):
        """Search the corresponding stock pack operations to fill it """
        # If the entrance is a partial entrance we need to clear the stock
        # moves tath won't be used in this operation to can use it on future
        # entrances
        if line.qty_done_processed == line.qty_done:
            move.stock_picking_interface_line_id = False
            return True
        spo_lot = self.env['stock.pack.operation.lot']
        line_qty_done = line.qty_done - line.qty_done_processed
        spo_ids = move.picking_id.pack_operation_product_ids.search(
            [('product_id', '=', line.product_id.id),
             ('picking_id', '=', move.picking_id.id)])
        for spo in spo_ids:
            spo_ordered_qty = spo.ordered_qty
            if line.need_serial_number and line.serial_number:
                serial_numbers = line.serial_number.split(',')
                serial_numbers.pop(-1)
                while serial_numbers:
                    serial_number = serial_numbers.pop(0)
                    if spo_lot.search(
                        [('operation_id.product_id', '=', move.product_id.id),
                         ('lot_name', '=', serial_number)]) or (
                            serial_number in spo.pack_lot_ids.mapped(
                                'lot_name')):
                        continue
                    if spo.qty_done == spo.ordered_qty:
                        break
                    spo_lot.create({
                        'lot_name': serial_number,
                        'operation_id': spo.id,
                    })
                    spo.qty_done += 1
                    spo_ordered_qty -= 1
                continue

            if spo_ordered_qty <= line_qty_done:
                spo.qty_done = spo_ordered_qty
                line_qty_done -= spo_ordered_qty
                line.qty_done_processed += spo_ordered_qty
            else:
                spo.qty_done = line_qty_done
                line.qty_done_processed += line_qty_done
                line_qty_done -= line_qty_done

    @api.model
    def prepare_stock_landed_cost(self):
        landed_cost_journal_id = (
            self.env.user.company_id.landed_cost_journal_id)
        if not landed_cost_journal_id:
            raise ValidationError(
                _('Error, you must configure a landed cost journal. \n'
                    'You can configure it in Stock > Configuration > Settings')
                )
        return {
            'date': self.date,
            'account_journal_id': landed_cost_journal_id.id,
            'picking_ids': [(6, 0, self.picking_ids.ids)],
        }

    @api.multi
    def action_validate(self, full_done=True):
        self.ensure_one()
        immediate_transfer_obj = self.env['stock.immediate.transfer']
        backorder_obj = self.env['stock.backorder.confirmation']
        stock_landed_cost_obj = self.env['stock.landed.cost']
        pickings_done = []
        pickings_pending = []
        for line in self.line_ids:
            # Get the moves ordered by a custom criteria
            move_ids = self.get_move_ordered(line)
            for move in move_ids:
                self.process_move_spo(line, move)
        # Validate the pickings
        for picking in self.picking_ids:
            if picking.check_backorder():
                wizard = backorder_obj.create({'pick_id': picking.id})
            else:
                wizard = immediate_transfer_obj.create({
                    'pick_id': picking.id,
                })

            wizard.process()
        # Create the landed cost with the pickings validated
        stock_landed_cost = stock_landed_cost_obj.create(
            self.prepare_stock_landed_cost())
        self.write({
            'state': 'done',
            'stock_landed_cost_id': stock_landed_cost.id,
        })
        for pick in self.line_ids.mapped('move_ids.picking_id'):
            if pick.state != 'done':
                pickings_pending.append(
                    "<a href=# data-oe-model=stock.picking"
                    " data-oe-id=%d target='_blank'>%s</a>" % (
                        pick.id, pick.name))
                continue
            pickings_done.append(
                "<a href=# data-oe-model=stock.picking"
                " data-oe-id=%d target='_blank'>%s</a>" % (
                    pick.id, pick.name))
        link_spi = (
            "<a href=# data-oe-model=stock.picking.interface"
            " data-oe-id=%d target='_blank'>%s</a>" % (
                self.id, self.name))
        msg = (
            "<ul>" +
            "<li>" + _("Done Pickings: ") + (
                " - ").join(pickings_done) + '</li>' +
            "<li>" + _("Pending Pickings: ") + (
                " - ").join(pickings_pending) + "</li>" +
            "<li>" + _("Stock Picking Interface: ") + link_spi + "</li>" +
            "</ul>")
        self.invoice_id.message_post(msg)
        self.message_post(msg)

    @api.multi
    def action_view_pickings(self):
        self.ensure_one()
        return {
            'name': _('Stock Picking(s)'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'domain': [(
                'id', 'in', self.picking_ids.ids)],
            'type': 'ir.actions.act_window',
            'context': {
                'create': False,
                'delete': False
            }
        }


class StockPickingInterfaceLine(models.Model):
    _name = 'stock.picking.interface.line'

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product',
        required=True,
        readonly=True,)
    ordered_qty = fields.Float(
        required=True,
        readonly=True, )
    qty_done = fields.Float(default=0.0)
    uom_id = fields.Many2one(
        comodel_name='product.uom',
        string='Unit of Measure',
        required=True,
        readonly=True, )
    stock_picking_interface_id = fields.Many2one(
        comodel_name='stock.picking.interface',
        string='Stock Picking Interface',
        readonly=True,
        ondelete='cascade',)
    move_ids = fields.One2many(
        comodel_name='stock.move',
        inverse_name='stock_picking_interface_line_id',
        string='Stock Move(s)',
        required=True,
        readonly=True,)
    serial_number = fields.Char()
    need_serial_number = fields.Boolean(
        help='Techincal field to determine if the line'
        ' need a serial number to be processed',
        compute='_compute_serial_number',)
    origin = fields.Char(
        help='Techincal field to show the origin purchase orders',
        compute='_compute_origin',
        store=True,)
    qty_done_processed = fields.Float(
        help='Technical field to store the processed quantity of the'
        ' stock moves during the spi confirm process.',)

    @api.multi
    def _compute_serial_number(self):
        for rec in self:
            rec.need_serial_number = rec.product_id.tracking == 'serial'

    @api.multi
    @api.depends('move_ids')
    def _compute_origin(self):
        for rec in self:
            rec.origin = (' - ').join(
                rec.move_ids.mapped(
                    'move_orig_ids.purchase_line_id.order_id.name'))
