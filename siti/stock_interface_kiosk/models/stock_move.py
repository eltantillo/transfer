# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class StockMove(models.Model):
    _inherit = 'stock.move'

    stock_picking_interface_line_id = fields.Many2one(
        comodel_name='stock.picking.interface.line',
        string='Stock Picking Interface Line',
        help='Technical field to link the invoice with the'
        ' picking interface line',)
