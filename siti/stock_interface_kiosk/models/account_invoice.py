# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    stock_picking_interface_id = fields.Many2one(
        comodel_name='stock.picking.interface',
        string='Stock Picking Interface',
        help='Technical field to link the invoice with the picking interface',
        copy=False,)
