# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Journal Entries Report for SITI",
    "version": "10.0.0.1.0",
    "author": "Jarsa Sistemas",
    "category": "Accouting",
    "website": "http://www.jarsa.com.mx/",
    "license": "AGPL-3",
    "depends": ["account"],
    "data": [
        "report/report.xml",
        "views/account_move_view.xml"
    ],
    "installable": True,
    "auto_install": False,
}
