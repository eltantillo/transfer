# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.

import datetime
from openerp import _, api, models
from openerp.exceptions import ValidationError


class AccountMove(models.Model):
    _inherit = 'account.move'

    today = datetime.date.today()

    @api.multi
    def print_account_move(self):
        try:
            return self.env['report'].get_action(
                self, 'account_move_report.account_move_report')
        except Exception:
            raise ValidationError(
                _("An error ocurred in the report please check."))
