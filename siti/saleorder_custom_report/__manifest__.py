# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Sale Order Report for sitibt',
    'version': '10.0.1.0.0',
    'category': 'Report',
    'author': 'Jarsa Sistemas',
    'license': 'AGPL-3',
    'depends': [
        'sale',
    ],
    'data': [
        'report/sale_order_report.xml',
    ],
    'installable': True,
}
