# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import datetime, timedelta
from odoo import models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def _get_delivery_date(self):
        for rec in self:
            if rec.confirmation_date:
                confirmation_date = datetime.strptime(
                    rec.confirmation_date, "%Y-%m-%d %H:%M:%S")
                for line in rec.order_line:
                    ft = confirmation_date + timedelta(
                        days=int(line.customer_lead))
        return ft
