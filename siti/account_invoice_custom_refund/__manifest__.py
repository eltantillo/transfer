# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Account Invoice Custom Refund',
    'version': '10.0.0.1.0',
    'category': 'Account',
    'author': 'Jarsa Sistemas',
    'summary': 'Create Custom Refunds',
    'license': 'AGPL-3',
    'depends': [
        'account'
    ],
    'data': [
        'wizards/account_invoice_refund_view.xml',
    ],
    'installable': True,
}
