# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def _prepare_refund(self, invoice, date_invoice=None, date=None,
                        description=None, journal_id=None):
        res = super(AccountInvoice, self)._prepare_refund(
            invoice, date_invoice, date, description, journal_id)
        # If a refund type is not selected the original dict is returned
        if not self._context.get('refund_type', False):
            return res

        refund_type = self._context['refund_type']
        refund_account = False

        if refund_type == 'return':
            # Return Account
            refund_account = self.env.ref('l10n_mx.1_cuenta_405_1001')
        else:
            # Discount Account
            refund_account = self.env.ref('l10n_mx.1_cuenta_406_1001')
        if not refund_account:
            return res

        for line in res['invoice_line_ids']:
            # The original account is changed for the new one
            line[2]['account_id'] = refund_account.id
        return res
