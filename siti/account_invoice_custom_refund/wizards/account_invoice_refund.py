# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class AccountInvoiceRefund(models.TransientModel):
    _inherit = 'account.invoice.refund'

    refund_type = fields.Selection(
        [('return', 'Return'),
         ('discount', 'Discount')],
        string='Type',
        default="return",
    )

    @api.multi
    def invoice_refund(self):
        # Add the refund type to the context
        if not self._context.get('refund_type', False):
            return super(AccountInvoiceRefund, self).with_context(
                refund_type=self.refund_type).invoice_refund()
        return super(AccountInvoiceRefund, self).invoice_refund()
