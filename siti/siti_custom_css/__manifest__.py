# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Custom CSS General Ledger for SITIBT',
    'version': '10.0.1.0.0',
    'category': 'Custom',
    'website': 'http://jarsa.com.mx',
    'author': 'Jarsa Sistemas',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'web',
    ],
    'data': [
        'views/css_rs.xml',
    ],
}
