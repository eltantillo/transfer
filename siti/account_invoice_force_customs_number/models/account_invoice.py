# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import _, api, models
from odoo.exceptions import ValidationError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def generate_customs_information(self):
        res = super(AccountInvoice, self).generate_customs_information()
        if self.mapped('invoice_line_ids').filtered(
                lambda l: not l.l10n_mx_edi_customs_number and
                l.product_id.type == 'stockable'):
            raise ValidationError(_(
                'You have products without Customs Number'))
        return res
