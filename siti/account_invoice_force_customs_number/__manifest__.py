# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Invoice Force Customs Number',
    'version': '10.0.1.0.0',
    'category': 'Account',
    'license': 'AGPL-3',
    'author': 'Jarsa Sistemas',
    'depends': [
        'l10n_mx_base'
    ],
    'installable': True,
}
