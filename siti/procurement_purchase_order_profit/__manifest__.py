# -*- coding: utf-8 -*-
# Copyright 2020, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Procurement Purchase Order Profit",
    "version": "10.0.0.1.0",
    "category": "purchase",
    "author": "Jarsa Sistemas",
    "license": 'AGPL-3',
    "depends": [
        "purchase",
        "sale",
        "stock"
    ],
    "data": [
        "security/res_groups.xml",
    ],
    "installable": True,
}
