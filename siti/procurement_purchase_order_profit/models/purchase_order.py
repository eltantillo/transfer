# -*- coding: utf-8 -*-
# Copyright 2017 Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import _, api, models
from odoo.exceptions import ValidationError


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def button_confirm(self):
        res = super(PurchaseOrder, self).button_confirm()
        sale_obj = self.env['sale.order']
        group = 'procurement_purchase_order_profit.res_groups_allow_po_confirm'
        for order in self:
            if order.group_id and not order.user_has_groups(group):
                sale = sale_obj.search([
                    ('name', '=', order.group_id.name)])
                for line in order.order_line:
                    sol = sale.order_line.filtered(
                        lambda sol: sol.product_id == line.product_id)
                    if sol and line.price_total >= sum(
                            sol.mapped('price_total')):
                        raise ValidationError(
                            _('You cannot confirm a Purchase Order'
                                ' without profit'))
        return res
