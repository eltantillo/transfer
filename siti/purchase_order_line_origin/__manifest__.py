# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Purchase Order Line Origin',
    'summary': 'Take Purchase Order origin to purchase line',
    'version': '10.0.1.0.0',
    'category': 'Purchase',
    'website': 'https://www.jarsa.com.mx/',
    'author': 'Jarsa Sistemas, S.A. de C.V.',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'purchase',
    ],
}
