# -*- coding: utf-8 -*-
# © <2017> <Jarsa Sistemas, S.A. de C.V.>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from openerp import fields, models


class ProductCustomer(models.Model):
    _name = 'product.customer'
    _description = 'Product customer information'

    partner_id = fields.Many2one(
        'res.partner',
        'Customer',
        required=True,
        domain=[('customer', '=', True)],)
    product_name = fields.Char()
    product_code = fields.Char()
    minimal_qty = fields.Integer(default=1.0,)
    delay = fields.Integer(default=1.0,)
    price = fields.Float(required=True, default=1.0,)
    product_tmpl_id = fields.Many2one('product.template', string='Product',)
    currency_id = fields.Many2one(
        'res.currency', string="Currency",
        required=True, default=lambda self: self.env.ref('base.USD'),)
