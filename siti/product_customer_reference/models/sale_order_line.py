# -*- coding: utf-8 -*-
# © <2017> <Jarsa Sistemas, S.A. de C.V.>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, models


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        res = super(SaleOrderLine, self).product_id_change()
        vals = {}
        if self.product_id:
            product = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.partner_id.id,
                quantity=self.product_uom_qty,
                date=self.order_id.date_order,
                uom=self.product_uom.id,
            )
            for line in product.customer_ids:
                if self.order_id.partner_id == line.partner_id:
                    self.name = (line.product_name if
                                 line.product_name else product.name)
                    vals['price_unit'] = self._get_display_price(product)
                self.update(vals)
        return res

    @api.multi
    def _get_display_price(self, product):
        res = super(SaleOrderLine, self)._get_display_price(product)
        currency_id = self.order_id.pricelist_id.currency_id
        for line in product.customer_ids:
            if self.order_id.partner_id == line.partner_id:
                return line.currency_id.compute(line.price, currency_id)
        return res
