# -*- coding: utf-8 -*-
# Copyright 2017 Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        obj_product_customer = self.env['product.customer']
        for order in self:
            partner_id = order.partner_id.id
            currency_id = order.pricelist_id.currency_id
            for order_line in order.order_line:
                product_id = order_line.product_id.product_tmpl_id.id
                customer_line = obj_product_customer.search([
                    ('partner_id', '=', partner_id),
                    ('product_tmpl_id', '=', product_id)])
                if customer_line:
                    customer_line.price = currency_id.compute(
                        order_line.price_total, customer_line.currency_id)
                else:
                    obj_product_customer.create({
                        'product_tmpl_id': (
                            order_line.product_id.product_tmpl_id.id),
                        'partner_id': partner_id,
                        'price': order_line.price_unit,
                        'currency_id': currency_id.id,
                    })
        return res
