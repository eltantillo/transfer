# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo.tests.common import TransactionCase


class TestSaleOrder(TransactionCase):

    def setUp(self):
        super(TestSaleOrder, self).setUp()
        self.partner_2 = self.env.ref("base.res_partner_2")
        self.partner_3 = self.env.ref("base.res_partner_3")
        self.pricelist = self.env.ref('product.list0')
        self.product = self.env.ref("product.product_product_10")
        self.customer_price = self.product.customer_ids.create({
            'product_tmpl_id': self.product.product_tmpl_id.id,
            'partner_id': self.partner_2.id,
            'product_code': "031216143",
            'price': 100,
            'currency_id': self.env.ref('base.MXN').id,
            })
        self.env['res.currency.rate'].search([]).unlink()
        self.env['res.currency.rate'].create({
            'rate': 20.00,
            'currency_id': self.env.ref('base.MXN').id,
        })

    def create_sale_order(self, partner_id):
        return self.env['sale.order'].create({
            'partner_id': partner_id.id,
            'pricelist_id': self.pricelist.id,
            'order_line': [(0, 0, {
                'product_id': self.product.id,
                'price_unit': 8.50})],
        })

    def test_10_sale_order_action_confirm(self):
        sale_order_1 = self.create_sale_order(self.partner_2)
        sale_order_1.action_confirm()
        price = self.customer_price.price
        self.assertEqual(price, 8.5)
        sale_order_2 = self.create_sale_order(self.partner_3)
        sale_order_2.action_confirm()
        customer_price2 = self.env['product.customer'].search([
            ('product_tmpl_id', '=', self.product.product_tmpl_id.id),
            ('partner_id', '=', self.partner_3.id)])
        self.assertEqual(customer_price2.currency_id.id,
                         self.pricelist.currency_id.id)
        self.assertEqual(customer_price2.price, 8.50)
