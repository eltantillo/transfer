# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo.tests.common import TransactionCase


class TestSaleOrderLine(TransactionCase):

    def setUp(self):
        super(TestSaleOrderLine, self).setUp()
        self.partner = self.env.ref("base.res_partner_2")
        self.product = self.env.ref("product.product_product_10")
        self.product.customer_ids.create({
            'product_tmpl_id': self.product.product_tmpl_id.id,
            'partner_id': self.partner.id,
            'product_code': "031216143",
            'price': 100,
            'currency_id': self.env.ref('base.MXN').id,
            })
        self.env['res.currency.rate'].search([]).unlink()
        self.env['res.currency.rate'].create({
            'rate': 20.00,
            'currency_id': self.env.ref('base.MXN').id,
        })

    def create_sale_order(self):
        return self.env['sale.order'].create({
            'partner_id': self.partner.id,
            'pricelist_id': self.env.ref('product.list0').id,
            'order_line': [(0, 0, {
                'product_id': self.product.id})],
        })

    def test_10_sale_order_line_product_id_change(self):
        sale_order = self.create_sale_order()
        price = sale_order.order_line.price_unit
        self.assertEqual(price, 100)
