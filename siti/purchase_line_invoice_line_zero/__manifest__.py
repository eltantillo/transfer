# -*- coding: utf-8 -*-
# © 2016 Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Purchase Line Invoice Line Zero',
    'version': '10.0.1.0.0',
    'category': 'Hidden',
    'website': 'https://www.jarsa.com.mx/',
    'author': 'Jarsa Sistemas',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'purchase',
    ],
}
