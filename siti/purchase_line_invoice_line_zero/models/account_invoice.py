# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


from odoo import api, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    # Load all unsold PO lines
    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if not self.purchase_id:
            return {}
        if not self.partner_id:
            self.partner_id = self.purchase_id.partner_id.id

        new_lines = self.env['account.invoice.line']
        for line in self.purchase_id.order_line - self.invoice_line_ids.mapped(
                'purchase_line_id'):
            data = self._prepare_invoice_line_from_po_line(line)
            if data['quantity'] > 0:
                new_line = new_lines.new(data)
                new_line._set_additional_fields(self)
                new_lines += new_line
        self.invoice_line_ids += new_lines
        self.purchase_id = False
        return {}
