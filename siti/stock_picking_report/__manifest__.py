# -*- coding: utf-8 -*-
# © 2017 Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Stock Picking Report",
    "version": "10.0.1.0.0",
    "category": "Hidden",
    "website": "https://www.jarsa.com.mx/",
    "author": "Jarsa Sistemas, S.A. de C.V.",
    "license": "AGPL-3",
    "installable": True,
    "depends": [
        'report',
        'stock',
        'sale'

    ],
    "data": [
        'report/stock_picking_report_id.xml',
        'views/report.xml',
    ],
    "demo": [
    ]
}
