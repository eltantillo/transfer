# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


from odoo import _, api, models
from odoo.exceptions import ValidationError


class StockPickingReport(models.AbstractModel):
    _name = 'report.stock_picking_report.stock_picking_report_id'

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name(
            'stock_picking_report.stock_picking_report_id')
        picking_ids = self.env[report.model].search([('id', 'in', docids)])
        for picking_id in picking_ids:
            if not picking_id.sale_id:
                raise ValidationError(
                    _('It is necessary that it be a warehouse exit in '
                      'order to be able to print this report.'))
            if picking_id.state == 'confirmed':
                raise ValidationError(
                    _('It is necessary to have product availability in '
                      'order to make an exit.'))
        docargs = {
            'doc_model': report.model,
            'doc_ids': docids,
            'docs': picking_ids,
            'product_lines': self.product_lines,
            'salesperson': self.salesperson,
            'get_tax_amount_by_group': self._get_tax_amount_by_group,
        }
        return report_obj.render(
            'stock_picking_report.stock_picking_report_id', docargs)

    @api.model
    def prepare_lines(self, line, product_qty):
        customer_code = "N/A"
        for customer in line.product_id.customer_ids:
            if line.order_id.partner_id == customer.partner_id:
                customer_code = customer.product_code
        return {
            'customer_code': customer_code,
            'code': line.product_id.default_code,
            'description': line.product_id.name,
            'quantity': product_qty,
            'unit_price': line.price_unit,
            'taxes': ', '.join(
                [x.description or x.name for x in line.tax_id]),
            'price': (
                product_qty *
                line.price_unit),
            }

    @api.model
    def product_lines(self, sale_id, picking_id):
        lines = []
        for pack_line in picking_id.pack_operation_product_ids:
            total_qty = pack_line.qty_done
            so_lines = sale_id.order_line.search([
                ('product_id', '=', pack_line.product_id.id),
                ('order_id', '=', sale_id.id)])
            if so_lines:
                for line_sale in so_lines:
                    if total_qty >= line_sale.product_uom_qty:
                        total_qty -= line_sale.product_uom_qty
                        lines.append(
                            self.prepare_lines(
                                line_sale, line_sale.product_uom_qty))
                    else:
                        lines.append(
                            self.prepare_lines(line_sale, total_qty))
                        break
        return lines

    @api.model
    def salesperson(self, picking_id):
        mails = self.env['mail.tracking.value'].search([
            ('mail_message_id', 'in', [x.id for x in picking_id.message_ids])])
        for mail in mails:
            if mail.new_value_char == _('Done'):
                return mail.mail_message_id.author_id.name

    @api.multi
    def _get_tax_amount_by_group(self, sale_id, picking_id):
        res = {}
        picking_ids = {
            line.product_id.id: line for line in
            picking_id.pack_operation_product_ids}
        products = []
        mrp_bom = self.env['mrp.bom']
        for line in sale_id.order_line:
            if (line.product_id.id in picking_ids.keys() and
                    line.product_id.id not in products):
                products.append(line.product_id.id)
                product = picking_ids[line.product_id.id]
                base_tax = 0
                for tax in line.tax_id:
                    group = tax.tax_group_id
                    res.setdefault(group, 0.0)
                    amount = tax.compute_all(
                        line.price_reduce + base_tax,
                        quantity=product.qty_done,
                        product=line.product_id,
                        partner=sale_id.partner_shipping_id
                    )['taxes'][0]['amount']
                    res[group] += amount
                    if tax.include_base_amount:
                        base_tax += tax.compute_all(
                            line.price_reduce + base_tax,
                            quantity=1, product=line.product_id,
                            partner=sale_id.partner_shipping_id
                        )['taxes'][0]['amount']
            elif any(key in picking_ids.keys() for key in mrp_bom.search(
                    [('product_tmpl_id', '=',
                        line.product_id.product_tmpl_id.id)]
                    ).bom_line_ids.mapped('product_id').ids):
                base_tax = 0
                for tax in line.tax_id:
                    group = tax.tax_group_id
                    res.setdefault(group, 0.0)
                    amount = tax.compute_all(
                        line.price_reduce + base_tax,
                        quantity=line.product_uom_qty,
                        product=line.product_id,
                        partner=sale_id.partner_shipping_id
                    )['taxes'][0]['amount']
                    res[group] += amount
                    if tax.include_base_amount:
                        base_tax += tax.compute_all(
                            line.price_reduce + base_tax,
                            quantity=1, product=line.product_id,
                            partner=sale_id.partner_shipping_id
                        )['taxes'][0]['amount']
        res = sorted(res.items(), key=lambda l: l[0].sequence)
        res = map(lambda l: (l[0].name, l[1]), res)  # pylint: disable=W0110
        return res
