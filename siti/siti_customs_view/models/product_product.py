# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


from odoo import api, fields, models
from odoo.addons import decimal_precision as dp


class ProductTemplate(models.Model):
    _inherit = 'product.product'

    qty_transit = fields.Float(
        'Quantity Transit', compute='_compute_quantities_product',
        digits=dp.get_precision('Product Unit of Measure'), store=True,)
    qty_physical = fields.Float(
        'Quantity Physical', compute='_compute_quantities_product',
        digits=dp.get_precision('Product Unit of Measure'), store=True,)
    qty_consignment = fields.Float(
        'Quantity Consignment', compute='_compute_quantities_product',
        digits=dp.get_precision('Product Unit of Measure'), store=True,)

    @api.depends('stock_move_ids.product_qty', 'stock_move_ids.state')
    def _compute_quantities_product(self):
        for rec in self:
            qty_transit = rec.domain_qty(
                'transit', rec.id).get(rec.id, 0.0)
            qty_physical = rec.domain_qty(
                'internal', rec.id).get(rec.id, 0.0)
            qty_consignment = rec.domain_qty(
                'internal', rec.id, True).get(rec.id, 0.0)
            rec.update({
                'qty_transit': qty_transit,
                'qty_physical': qty_physical,
                'qty_consignment': qty_consignment,
            })
        return True

    @api.model
    def domain_qty(self, type_location, product_id, consignment=False):
        quant = self.env['stock.quant']
        quants_res = dict(
            (item['product_id'][0], item['qty']) for item in quant.read_group([
                ('location_id.usage', '=', type_location),
                ('location_id.consignment', '=', consignment),
                ('product_id', '=', product_id)],
                ['product_id', 'qty'], ['product_id']))
        return quants_res

    @api.multi
    def action_open_quants_transit(self):
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', '=', self.id),
                            ('location_id.usage', '=', 'transit')]
        action['context'] = {
            'search_default_locationgroup': 1,
            'search_default_internal_loc': 0,
        }
        return action

    @api.multi
    def action_open_quants_physical(self):
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', '=', self.id),
                            ('location_id.usage', '=', 'internal'),
                            ('location_id.consignment', '=', False)]
        action['context'] = {
            'search_default_locationgroup': 1,
            'search_default_internal_loc': 1,
        }
        return action

    @api.multi
    def action_open_quants_consignment(self):
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', '=', self.id),
                            ('location_id.usage', '=', 'internal'),
                            ('location_id.consignment', '=', True)]
        action['context'] = {
            'search_default_locationgroup': 1,
            'search_default_internal_loc': 1,
        }
        return action
