# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


from odoo import api, fields, models
from odoo.addons import decimal_precision as dp


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    qty_transit = fields.Float(
        'Quantity Transit', compute='_compute_quantities_product',
        digits=dp.get_precision('Product Unit of Measure'), store=True,)
    qty_physical = fields.Float(
        'Quantity Physical', compute='_compute_quantities_product',
        digits=dp.get_precision('Product Unit of Measure'), store=True,)
    qty_consignment = fields.Float(
        'Quantity Consignment', compute='_compute_quantities_product',
        digits=dp.get_precision('Product Unit of Measure'), store=True,)

    @api.multi
    @api.depends(
        'product_variant_ids.qty_transit', 'product_variant_ids.qty_physical',
        'product_variant_ids.qty_consignment')
    def _compute_quantities_product(self):
        res = self._compute_quantities_dict_product()
        for template in self:
            template.qty_transit = res[template.id]['qty_transit']
            template.qty_physical = res[template.id]['qty_physical']
            template.qty_consignment = res[template.id]['qty_consignment']

    def _compute_quantities_dict_product(self):
        prod_available = {}
        obj_product = self.env['product.product']
        for template in self:
            variant_id = template.mapped('product_variant_id').id
            variants_transit = obj_product.domain_qty(
                'transit', variant_id, False)
            variants_physical = obj_product.domain_qty(
                'internal', variant_id, False)
            variants_consignment = obj_product.domain_qty(
                'internal', variant_id, True)
            qty_transit = 0
            qty_physical = 0
            qty_consignment = 0
            for product in template.product_variant_ids:
                if variants_transit:
                    qty_transit += variants_transit.get(product.id, 0)
                if variants_physical:
                    qty_physical += variants_physical.get(product.id, 0)
                if variants_consignment:
                    qty_consignment += variants_consignment.get(product.id, 0)
            prod_available[template.id] = {
                "qty_transit": qty_transit,
                "qty_physical": qty_physical,
                "qty_consignment": qty_consignment,
            }
        return prod_available

    @api.multi
    def action_open_quants_transit(self):
        products = self.mapped('product_variant_id')
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', 'in', products.ids),
                            ('location_id.usage', '=', 'transit')]
        action['context'] = {
            'search_default_locationgroup': 1,
            'search_default_internal_loc': 0,
        }
        return action

    @api.multi
    def action_open_quants_physical(self):
        products = self.mapped('product_variant_id')
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', 'in', products.ids),
                            ('location_id.usage', '=', 'internal'),
                            ('location_id.consignment', '=', False)]
        action['context'] = {
            'search_default_locationgroup': 1,
            'search_default_internal_loc': 1,
        }
        return action

    @api.multi
    def action_open_quants_consignment(self):
        products = self.mapped('product_variant_id')
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', 'in', products.ids),
                            ('location_id.usage', '=', 'internal'),
                            ('location_id.consignment', '=', True)]
        action['context'] = {
            'search_default_locationgroup': 1,
            'search_default_internal_loc': 1,
        }
        return action
