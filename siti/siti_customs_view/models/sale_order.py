# -*- coding: utf-8 -*-
# Copyright 2020 Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import _, api, models
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        utility_percentage = self.env['ir.config_parameter'].get_param(
            'utility_percentage')
        group = 'siti_customs_view.res_groups_utility_percentage'
        for order in self:
            for line in order.order_line:
                if line.product_id.standard_price != 0.0:
                    line_cost = line.product_id.standard_price
                    utility = ((line.price_unit / line_cost) - 1) * 100
                    if utility < float(utility_percentage) and not (
                            order.user_has_groups(group)):
                        raise ValidationError(
                            _('The utility is less than the '
                                ' expected in the percentage'))
        return res
