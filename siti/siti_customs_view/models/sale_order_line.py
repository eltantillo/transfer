# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, models
from odoo.tools import float_compare
from odoo.tools.translate import _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.model
    def prepare_warning(self, value):
        return{
            'title': _('Not enough inventory!'),
            'message': _(
                'You plan to sell %s %s but you only have'
                ' %s %s available!\nThe stock on hand is %s %s.') %
            (self.product_uom_qty, self.product_uom.name, value,
             self.product_id.uom_id.name, value, self.product_id.uom_id.name)
        }

    @api.onchange('product_uom_qty', 'product_uom', 'route_id')
    def _onchange_product_id_check_availability(self):
        super(SaleOrderLine, self)._onchange_product_id_check_availability()
        if (not self.product_id or not self.product_uom_qty or not
                self.product_uom):
            self.product_packaging = False
            return {}
        if self.product_id.type == 'product':
            precision = self.env['decimal.precision'].precision_get(
                'Product Unit of Measure')
            product_qty = self.product_uom._compute_quantity(
                self.product_uom_qty, self.product_id.uom_id)
            # If the sale order line has a route the qty to check will
            # be the qty on consignment
            if self.route_id:
                if float_compare(
                        self.product_id.qty_consignment, product_qty,
                        precision_digits=precision) == -1:
                    return {'warning': self.prepare_warning(
                        self.product_id.qty_consignment)}
                return {}
            # If the sale order line has a route the qty to check will
            # be the qty on consignment
            if float_compare(
                    self.product_id.qty_physical, product_qty,
                    precision_digits=precision) == -1:
                return {'warning': self.prepare_warning(
                    self.product_id.qty_physical)}
        return {}
