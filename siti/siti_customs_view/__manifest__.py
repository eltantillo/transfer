# -*- coding: utf-8 -*-
# Copyright 2017 Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Siti Custom Views",
    "version": "10.0.1.0.0",
    "category": "Customs",
    "website": "https://www.jarsa.com.mx/",
    "author": "Jarsa Sistemas, S.A. de C.V.",
    "license": "AGPL-3",
    "installable": True,
    "depends": [
        'sale_stock',
        'purchase',
        'stock_interface_kiosk',
    ],
    "data": [
        'security/res_groups.xml',
        'security/ir.model.access.csv',
        'views/stock_picking_view.xml',
        'views/product_view.xml',
        'views/stock_move_view.xml',
        'views/stock_quant.xml',
        'views/account_invoice.xml',
        'views/stock_location_view.xml',
        'data/ir_config_parameter_data.xml',
    ]
}
