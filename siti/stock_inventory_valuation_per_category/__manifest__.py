# -*- coding: utf-8 -*-
# © 2017 Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Stock Inventory Valuation Per Category',
    'summary': 'Inventory Valuation Per Category',
    'version': '10.0.1.0.0',
    'category': 'Stock',
    'website': 'https://www.jarsa.com.mx/',
    'author': 'Jarsa Sistemas, S.A. de C.V.',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'stock',
    ],
    'data': [
        'views/stock_quant_view.xml',
    ],
    'demo': []
}
