# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "Siti no editable Custom Views",
    "summary": "Custom views for siti",
    "version": "10.0.0.1.0",
    "website": "https://www.jarsa.com.mx/",
    "author": "Jarsa Sistemas, S.A. de C.V.",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "account",
        "product",
        "purchase",
        "sale",
    ],
    "data": [
        "views/no_editable_view.xml"
    ],
}
