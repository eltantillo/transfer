# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'SITI Security Module',
    'summary': 'Security rules for SITI',
    'version': '10.0.1.0.1',
    'category': 'security',
    'website': 'https://www.jarsa.com.mx',
    'author': 'Jarsa Sistemas, Odoo Community Association (OCA)',
    'license': 'AGPL-3',
    'application': False,
    'installable': True,
    'depends': [
        'l10n_mx_base',
        'stock_landed_costs',
        'website_sale',
        'mrp',
    ],
    'data': [
        'data/groups.xml',
        'views/picking_buttons.xml',
        'views/stock_landed_cost.xml',
        'security/ir.model.access.csv',
    ],
}
