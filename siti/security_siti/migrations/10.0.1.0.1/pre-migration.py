# -*- coding: utf-8 -*-
# Copyright 2020, Jarsa Sistemas S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


def migrate(cr, version):
    cr.execute("""
        UPDATE ir_model_data
        SET
        module = 'security_siti',
        name = 'can_create_products'
        WHERE
        model = 'res.groups'
        AND
        res_id = 60
    """)
    cr.execute("""
        UPDATE ir_model_data
        SET
        module = 'security_siti',
        name = 'create_product_template'
        WHERE
        model = 'ir.model.access'
        AND
        res_id = 612
    """)
    cr.execute("""
        UPDATE ir_model_data
        SET
        module = 'security_siti',
        name = 'create_product_product'
        WHERE
        model = 'ir.model.access'
        AND
        res_id = 611
    """)
