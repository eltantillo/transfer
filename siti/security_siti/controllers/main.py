# -*- coding: utf-8 -*-
# Copyright 2019, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import http
from odoo.addons.website_sale.controllers.main import WebsiteSale


class WebsiteSaleSiti(WebsiteSale):

    @http.route(auth="user")
    def shop(self, page=0, category=None,
             search='', ppg=False, **post):
        return super(WebsiteSaleSiti, self).shop(
            page, category, search, ppg, **post)

    @http.route(auth="user")
    def product(self, product, category='', search='', **kwargs):
        return super(WebsiteSaleSiti, self).product(
            product, category, search, **kwargs)
