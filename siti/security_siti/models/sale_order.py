# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import _, api, models
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_cancel(self):
        if self.mapped('picking_ids').filtered(lambda r: r.state == 'done'):
            raise ValidationError(
                _("This sale order can not ve canceled because "
                  "it has almost one stock move in state done"))
        return super(SaleOrder, self).action_cancel()
