# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Account Partner Currency Statement',
    'summary': 'Partner Balance in Origin Currency',
    'version': '10.0.1.0.0',
    'category': 'Account',
    'website': 'https://www.jarsa.com.mx/',
    'author': 'Jarsa Sistemas',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'account',
        'sale',
    ],
    'data': [
        'data/paperformat.xml',
        'report/account_partner_currency_template.xml',
        'wizards/account_partner_currency_statement_wizard_view.xml',
    ],
}
