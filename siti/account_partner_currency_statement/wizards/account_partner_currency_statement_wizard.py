# -*- coding: utf-8 -*-
# Copyright 2018, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class PartnerStatementWizard(models.TransientModel):
    _name = 'partner.balance.currency.wizard'

    date_end = fields.Date(
        default=fields.Date.today,
        required=True,
    )
    currency_id = fields.Many2one(
        comodel_name='res.currency',
        string="Currency",
        default=lambda self: self.env.user.company_id.currency_id,
        required=True,
    )
    company_id = fields.Many2one(
        comodel_name='res.company',
        string="Company",
        default=lambda self: self.env.user.company_id,
    )

    @api.model
    def get_amls(self):
        self.ensure_one()
        amls = {}
        currency_clause = 'AND aml.currency_id IS NULL'
        if (self.currency_id and self.currency_id !=
                self.env.user.company_id.currency_id):
            currency_clause = 'AND aml.currency_id = ' + str(
                self.currency_id.id)
        query = (
            """SELECT aml.id, aml.invoice_id, aml.partner_id,
                aml.balance, aml.name, aml.amount_currency, aml.journal_id
            FROM account_move_line aml
            JOIN account_account aa ON aa.id = aml.account_id AND aa.reconcile
            JOIN account_journal aj ON aj.id = aml.journal_id
            WHERE aml.date <= %s
                AND aj.type != %s
                %s
            ORDER BY aml.partner_id""")
        self._cr.execute(query, (self.date_end, 'bank', currency_clause))
        data = self._cr.dictfetchall()
        for rec in data:
            if rec['partner_id'] not in amls:
                amls[rec['partner_id']] = []
            amls[rec['partner_id']].append(rec)
        return amls

    @api.model
    def get_partner_info(self):
        partner_obj = self.env['res.partner']
        invoice_obj = self.env['account.invoice']
        aml_obj = self.env['account.move.line']
        res = {}
        data = self.get_amls()
        for amls in data.values():
            for aml in amls:
                partner = partner_obj.browse(aml['partner_id']).name
                if aml['journal_id'] == 4:
                    continue
                balance = (
                    abs(aml['balance']) if not aml['amount_currency'] else
                    abs(aml['amount_currency']))
                invoice = invoice_obj.browse(aml['invoice_id'])
                name = aml['name']
                if invoice:
                    name = invoice.number
                    partner = (
                        invoice.partner_shipping_id.name if
                        invoice.partner_shipping_id else partner)
                self._cr.execute(
                    """
                    SELECT CASE WHEN pr.debit_move_id = %s THEN
                        pr.credit_move_id ELSE pr.debit_move_id END
                        AS payment_aml, pr.amount, pr.amount_currency,
                        currency_id
                    FROM account_partial_reconcile pr
                    WHERE pr.credit_move_id = %s OR pr.debit_move_id = %s""",
                    (aml['id'], aml['id'], aml['id']))
                partials = self._cr.dictfetchall()
                for partial in partials:
                    bnk_aml = aml_obj.browse(partial['payment_aml'])
                    if bnk_aml.date <= self.date_end:
                        balance -= (
                            abs(partial['amount']) if not
                            partial['amount_currency'] else
                            abs(partial['amount_currency']))
                if balance > 0.0:
                    if partner not in res:
                        res[partner] = [0.0, []]
                    res[partner][1].append({
                        'name': name,
                        'amount': balance,
                    })
                    res[partner][0] += balance
        return res

    @api.multi
    def print_report(self):
        self.ensure_one()
        return self.env['report'].get_action(
            self, 'account_partner_currency_statement.'
            'account_partner_currency_template')
