.. image:: https://img.shields.io/badge/licence-LGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/lgpl-3.0-standalone.html
   :alt: License: LGPL-3

SitiBT Base Module
==================

This module build and install the sitibt custom modules.

Maintainer
----------

.. image:: http://www.jarsa.com.mx/logo.png
   :alt: Jarsa
   :target: http://www.jarsa.com.mx

This module is maintained by Jarsa
