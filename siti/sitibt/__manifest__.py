# -*- coding: utf-8 -*-
# Copyright 2020, Jarsa
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    'name': 'SitiBT Instance',
    'summary': 'Instance module',
    'version': '10.0.1.0.0',
    'category': 'Instance',
    'website': 'https://www.jarsa.com.mx/',
    'author': 'Jarsa Sistemas',
    'license': 'LGPL-3',
    'depends': [
        'account_invoice_custom_refund',
        'account_invoice_force_customs_number',
        'account_move_report',
        'account_partner_currency_statement',
        'account_reconciliation_uuid',
        'invoice_custom_report',
        'product_customer_reference',
        'purchase_line_invoice_line_zero',
        'purchase_order_line_origin',
        'report_out_label',
        'sale_line_to_invoice_line_ref',
        'saleorder_custom_report',
        'security_siti',
        'siti_custom_css',
        'siti_custom_view_noeditable',
        'siti_customs_view',
        'stock_interface_kiosk',
        'stock_inventory_valuation_per_category',
        'stock_picking_report',
        'company_country',
    ],
}
