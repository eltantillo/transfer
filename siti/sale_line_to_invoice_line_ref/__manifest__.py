# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Sale Order Line to Invoice line reference',
    'summary': 'Take Sale Order reference to invoice line',
    'version': '10.0.1.0.0',
    'category': 'Sale',
    'website': 'https://www.jarsa.com.mx/',
    'author': 'Jarsa Sistemas, S.A. de C.V.',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'sale',
        'account',
    ],
}
