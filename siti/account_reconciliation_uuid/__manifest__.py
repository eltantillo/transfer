# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Account Reconciliation CFDI UUID",
    "summary": "Show the CFDI UUID in the reconciliation sheet",
    "version": "10.0.0.1.0",
    "category": "Accounting",
    "author": "Jarsa Sistemas",
    "website": "https://www.jarsa.com.mx",
    "depends": ['account'],
    "license": "AGPL-3",
    "data": [
        'views/account.xml',
    ],
    "installable": True,
}
