# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, models


class AccountInvoice(models.Model):
    """ Inherit from account invoice to account reconciliation """
    _inherit = 'account.invoice'

    @api.model
    def search_cfdi_uuid(self, line):
        move_line = self.env['account.move.line'].browse(line['id'])
        if move_line.journal_id.type == 'bank':
            return self.search(
                [('move_name', '=', line['ref'])], limit=1).cfdi_uuid or ''
        invoice_name = line['name'].split(':')[0]
        return self.search(
            [('move_name', '=', invoice_name)], limit=1).cfdi_uuid or ''
