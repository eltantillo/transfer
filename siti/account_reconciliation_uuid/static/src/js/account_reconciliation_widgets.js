odoo.define('account.reconciliation.cfdi.uuid', function (require) {
    "use strict";
    var core = require('web.core');
    var Model = require('web.Model');
    var AccountReconciliation = require('account.reconciliation');
    var QWeb = core.qweb;


    var abstractReconciliation2 = AccountReconciliation.abstractReconciliation.include({
        decorateMoveLine: function (line) {
            this._super(line);
            var self = this;
            self.line = line;
            new Model("account.invoice").call('search_cfdi_uuid', [line]).then(
                function (data) {
                    if (data.length > 0) {
                        self.line.q_label += ' - ' + data;
                        var template_name = (
                            QWeb.has_template(
                                this.template_prefix +
                                "reconciliation_move_line_details"
                            ) ? this.template_prefix : ""
                        ) + "reconciliation_move_line_details";
                        self.line.q_popover = QWeb.render(
                            template_name, {line: self.line});
                        self.updateAccountingViewMatchedLines();
                    }
                });
        },
    });
});
