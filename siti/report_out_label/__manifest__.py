# -*- coding: utf-8 -*-
# Copyright 2017, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Report of exit',
    'version': '10.0.1.0.0',
    'category': 'Initial Data',
    'author': 'Jarsa Sistemas',
    'license': 'AGPL-3',
    'depends': [
        'stock',
        'base',
        'report',
        'web',
        'sale',
        'purchase'
    ],
    'data': [
        'report/report_out_label.xml',
        'views/report.xml',
    ],
    'installable': True,
}
