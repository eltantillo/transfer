# -*- coding: utf-8 -*-
# Copyright 2016, Jarsa Sistemas, S.A. de C.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


from odoo import api, models


class ReportExit(models.AbstractModel):
    _name = 'report.report_out_label.report_stock'

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name(
            'report_out_label.report_stock')
        active_ids = self.env[report.model].search([
            ('id', 'in', docids)])
        docargs = {
            'doc_model': report.model,
            'doc_ids': docids,
            'docs': active_ids,
            'client_order_ref': self.get_client_order_ref
        }
        return report_obj.render(
            'report_out_label.report_stock', docargs)

    @api.model
    def get_client_order_ref(self, picking):
        return self.env['sale.order'].search([
            ('name', '=', picking.group_id.name)]).client_order_ref
