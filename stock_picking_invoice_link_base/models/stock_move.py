# Copyright 2013-15 Agile Business Group sagl (<http://www.agilebg.com>)
# Copyright 2015-2016 AvanzOSC
# Copyright 2016 Pedro M. Baeza <pedro.baeza@tecnativa.com>
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

from odoo import _, fields, models
from odoo.exceptions import UserError


class StockMove(models.Model):
    _inherit = "stock.move"

    invoice_line_ids = fields.Many2many(
        comodel_name="account.move.line",
        relation="stock_move_invoice_line_rel",
        column1="move_id",
        column2="invoice_line_id",
        string="Invoice Line",
        copy=False,
        readonly=True,
    )

    def _action_done(self, cancel_backorder=False):
        moves_todo = super(StockMove, self)._action_done(cancel_backorder=cancel_backorder)
        moves_todo._action_propagate_valuation()
        return moves_todo

    def _action_propagate_valuation(self):
        for move in self:
            # las ventas tienen signo negativo, por ello multiplicar x -1
            # las devoluciones al ser entradas tendrian signo positivo y al multiplicar por -1 quedan en negativo
            move_value = sum(move.stock_valuation_layer_ids.mapped("value")) * -1
            if move.invoice_line_ids:
                for invoice_line in move.invoice_line_ids:
                    if invoice_line._can_update_amount_cost():
                        invoice_line.write({"amount_cost": invoice_line.amount_cost + move_value})
        return True

    def write(self, vals):
        """
        User can update any picking in done state, but if this picking already
        invoiced the stock move done quantities can be different to invoice
        line quantities. So to avoid this inconsistency you can not update any
        stock move line in done state and have invoice lines linked.
        """
        if "product_uom_qty" in vals and not self.env.context.get(
            "bypass_stock_move_update_restriction"
        ):
            for move in self:
                if move.state == "done" and move.invoice_line_ids:
                    raise UserError(_("You can not modify an invoiced stock move"))
        return super().write(vals)
